import random
import time
import pygame
import threading
import sort as s

l = []
estFini = False
PAUSE = 0.00005
# PAUSE = 0

def setup():
    global l
    global estFini
    l = [random.randint(0, WINDOWSIZE[1]) for i in range(0, WINDOWSIZE[0])]
    estFini = False
    t1 = threading.Thread(target=triage, args=())
    t1.start()


def triage():
    global l
    global estFini
    s.heapSort(l, PAUSE)
    estFini = True

WINDOWSIZE = (1000, 500)

pygame.init()
fenetre = pygame.display.set_mode(WINDOWSIZE)
pygame.display.set_caption("sort")

time_per_frame = 1 / 60
last_time = time.time()
frameCount = 0

setup()

run = True
while run:
    delta_time = time.time() - last_time
    while delta_time < time_per_frame:
        delta_time = time.time() - last_time

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

            keys = pygame.key.get_pressed()
            if keys[pygame.K_ESCAPE]:
                run = False
            elif keys[pygame.K_F5]:
                setup()


        fenetre.fill((0, 0, 0))
        if estFini == False:
            for index, value in enumerate(l):
                pygame.draw.line(fenetre, (255, 255, 255), (index, WINDOWSIZE[1]-value), (index, WINDOWSIZE[1]))
            pygame.display.update()
        elif estFini == True:
            for index, value in enumerate(l):
                pygame.draw.line(fenetre, (0, 255, 0), (index, WINDOWSIZE[1]-value), (index, WINDOWSIZE[1]))
                pygame.display.update()
            pygame.draw.line(fenetre, (255, 0, 0), (0, WINDOWSIZE[1]), (WINDOWSIZE[0], 0), 2)
            pygame.display.update()
            estFini = None

    last_time = time.time()
    frameCount += 1
