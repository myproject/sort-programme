from time import sleep
import functionForSort as f
#TODO: quickSort (RecursionError: maximum recursion depth exceeded in comparison)
#TODO: merge sort ne peut pas etres dessiner en temps reel
#TODO: bitonicSort ne range pas tout a fait bien

def selectionSort(l, pause):
    for i in range(len(l)):
        sleep(pause)
        min_idx = i
        for j in range(i + 1, len(l)):
            if l[min_idx] > l[j]:
                min_idx = j

        l[i], l[min_idx] = l[min_idx], l[i]

def insertionSort(l, pause):
    for i in range(1, len(l)):
        key = l[i]
        sleep(pause)

        # Move elements of arr[0..i-1], that are
        # greater than key, to one position ahead
        # of their current position
        j = i - 1
        while j >= 0 and key < l[j]:
            l[j + 1] = l[j]
            j -= 1
        l[j + 1] = key

def heapSort(arr, pause):
    n = len(arr)

    # Build a maxheap.
    # Since last parent will be at ((n//2)-1) we can start at that location.
    for i in range(n // 2 - 1, -1, -1):
        f.heapify(arr, n, i)

    # One by one extract elements
    for i in range(n - 1, 0, -1):
        sleep(pause)
        arr[i], arr[0] = arr[0], arr[i]  # swap
        f.heapify(arr, i, 0)


def radixSort(arr, pause):
    # Find the maximum number to know number of digits
    max1 = max(arr)

    # Do counting sort for every digit. Note that instead
    # of passing digit number, exp is passed. exp is 10^i
    # where i is current digit number
    exp = 1
    while max1 / exp > 1:
        f.countingSort(arr, exp, pause)
        exp *= 10


def shellSort(arr, pause):
    # Start with a big gap, then reduce the gap
    n = len(arr)
    gap = round(n / 2)

    # Do a gapped insertion sort for this gap size.
    # The first gap elements a[0..gap-1] are already in gapped
    # order keep adding one more element until the entire array
    # is gap sorted
    while gap > 0:
        print(gap)
        for i in range(gap, n):
            sleep(pause)

            # add a[i] to the elements that have been gap sorted
            # save a[i] in temp and make a hole at position i
            temp = arr[i]

            # shift earlier gap-sorted elements up until the correct
            # location for a[i] is found
            j = i
            while j >= gap and arr[j - gap] > temp:
                arr[j] = arr[j - gap]
                j -= gap

            # put temp (the original a[i]) in its correct location
            arr[j] = temp
        gap = round(gap / 2)


def bubbleSort(arr, pause):
    n = len(arr)

    # Traverse through all array elements
    for i in range(n - 1):
        sleep(pause)
        # range(n) also work but outer loop will
        # repeat one time more than needed.

        # Last i elements are already in place
        for j in range(0, n - i - 1):

            # traverse the array from 0 to n-i-1
            # Swap if the element found is greater
            # than the next element
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]


def cocktailSort(a, pause):
    n = len(a)
    swapped = True
    start = 0
    end = n - 1
    while (swapped == True):
        sleep(pause)
        # reset the swapped flag on entering the loop,
        # because it might be true from a previous
        # iteration.
        swapped = False

        # loop from left to right same as the bubble
        # sort
        for i in range(start, end):
            if (a[i] > a[i + 1]):
                a[i], a[i + 1] = a[i + 1], a[i]
                swapped = True

        # if nothing moved, then array is sorted.
        if (swapped == False):
            break

        # otherwise, reset the swapped flag so that it
        # can be used in the next stage
        swapped = False

        # move the end point back by one, because
        # item at the end is in its rightful spot
        end = end - 1

        # from right to left, doing the same
        # comparison as in the previous stage
        for i in range(end - 1, start - 1, -1):
            if (a[i] > a[i + 1]):
                a[i], a[i + 1] = a[i + 1], a[i]
                swapped = True

        # increase the starting point, because
        # the last stage would have moved the next
        # smallest number to its rightful spot.
        start = start + 1


def gnomeSort(arr, pause):
    n = len(arr)
    index = 0
    while index < n:
        if index == 0:
            index = index + 1
        if arr[index] >= arr[index - 1]:
            index = index + 1
        else:
            sleep(pause)
            arr[index], arr[index - 1] = arr[index - 1], arr[index]
            index = index - 1


def bogoSort(a, pause):
    n = len(a)
    while f.is_sorted(a) == False:
        sleep(pause)
        f.shuffle(a)